[19:01:09] Meeting started by prometheanfire
[19:01:58] Meeting chairs are: alicef, zlg, prometheanfire, dabbott, kensington, 
[19:02:12] Current subject: rollcall, (set by prometheanfire)
[19:02:15] <prometheanfire> o/
[19:02:17] <zlg> o/
[19:02:19] <robbat2> present
[19:02:20] <alicef> 0/
[19:02:28] <kensington> O/
[19:02:29] <alicef> o/
[19:02:30] <dabbott> here
[19:02:47] <prometheanfire> k, that's all of us I think
[19:02:57] <prometheanfire> or all the now current trustees
[19:03:03] <prometheanfire> dabbott: you logging?
[19:03:07] <dabbott> yes
[19:03:10] <robbat2> swift: ping
[19:03:11] <prometheanfire> cool
[19:03:11] <zlg> is also logging.
[19:03:17] <robbat2> i don't see antarus
[19:03:26] <prometheanfire> is always logging, but doesn't publish
[19:03:34] <prometheanfire> robbat2: ya, haven't seen him in a bit
[19:04:29] <shentino> I'm here
[19:05:06] <prometheanfire> anyway, on to old business first
[19:05:10] <prometheanfire> the election :D
[19:05:22] <prometheanfire> congrats to alicef zlg and kensington 
[19:05:26] <prometheanfire> also, I'm sorry
[19:05:35] <zlg> What are you sorry for?
[19:06:37] <prometheanfire> more work for you
[19:06:45] <prometheanfire> it's a joke
[19:06:54] <zlg> I think we ran understanding it was going to be some work. :P
[19:07:02] <prometheanfire> :P
[19:07:19] <prometheanfire> moving on
[19:07:29] Current subject: secretary report, (set by prometheanfire)
[19:07:37] LINK: https://wiki.gentoo.org/wiki/Foundation:2017_Secretary_Report [Foundation:2017 Secretary Report - Gentoo Wiki]
[19:07:48] <prometheanfire> dabbott: your turn :D
[19:08:23] <dabbott> welcome alicef kensington and zlg 
[19:08:52] <alicef> thanks :)
[19:08:55] <zlg> Thanks for the honor. :)
[19:08:57] <dabbott> secretary report just some general links etc
[19:09:15] <zlg> I have the page pulled up. Is this something the secretary does for each AGM?
[19:09:27] <dabbott> I have been
[19:10:30] <dabbott> next year hope to have the financial info
[19:11:06] <prometheanfire> yep, speaking of...
[19:11:11] <zlg> If things go well with robbat2 and me, that is something I'd like to get done.
[19:11:11] <dabbott> thats all from me
[19:11:17] <robbat2> can you come back to me in a few?
[19:11:21] <prometheanfire> robbat2: sure
[19:11:25] <robbat2> had a toddler IRQ in my writeup
[19:11:48] <zlg> robbat2: Have you used the toy scheduler?
[19:11:48] <prometheanfire> rich0: ping
[19:11:59] <rich0> prometheanfire: pong
[19:12:09] Current subject: rich0 stepping down from officer role as Assistant Secretary, (set by prometheanfire)
[19:12:19] <prometheanfire> guess it's more of a notice than anything
[19:12:32] <prometheanfire> but if that's all for that we can move on
[19:12:39] <rich0> wfm :)
[19:12:46] <dabbott> We just wanted to try and keep him involved 
[19:13:15] <prometheanfire> sure
[19:13:24] <zlg> Seems smart to keep institutional knowledge around for the new blood.
[19:13:27] Current subject: Determine which new trustees do what., (set by prometheanfire)
[19:13:47] <prometheanfire> zlg: it seems you are training under robbat2 to take on the financials
[19:13:48] <alicef> there is any list of task ?
[19:14:00] <prometheanfire> alicef: and you are training under dabbott?
[19:14:05] <alicef> yes
[19:14:18] <zlg> prometheanfire: That's what I understand, yes. We haven't hashed anything out yet as to scheduling, but that's just a matter of communication.
[19:14:33] <prometheanfire> cool
[19:14:51] <prometheanfire> alicef: as far as tasks, it's basically the meeting agenda and https://wiki.gentoo.org/wiki/Foundation:Activity_Tracker
[19:14:57] <dabbott> alicef: I will send you an email in the next few days
[19:15:00] <alicef> I will try to keep up with dabbot work
[19:15:01] <prometheanfire> alicef: for more details ask dabbott :D
[19:15:10] <prometheanfire> k, moving on
[19:15:42] Current subject: https://wiki.gentoo.org/wiki/Foundation:Activity_Tracker, (set by prometheanfire)
[19:16:11] <dabbott> do the new trustees have permission on the foundation namespace on the wiki?
[19:16:22] <zlg> checks.
[19:16:34] <prometheanfire> guess the only two things on there that we haven't gone over yet are the Treasurer's and Presidents reports
[19:17:26] <zlg> When I login to the wiki, I am unable to edit the Activity Tracker page that was just linked. I assume permissions haven't been set yet.
[19:17:33] <alicef> me too
[19:17:47] <prometheanfire> I wonder if it's just the members listed under https://wiki.gentoo.org/wiki/Foundation:Main_Page
[19:18:32] <dabbott> plus there are 3 git repo's we use the project, financial and a private repo afair
[19:18:59] <prometheanfire> I'm not sure we have a full hadover document, would be good to have
[19:19:02] <dabbott> maffblaster: would be able to help with the wiki
[19:19:18] <dabbott> prometheanfire: yes for sure
[19:19:21] <prometheanfire> I'll update the member list
[19:19:33] <robbat2> (i'm ready on on treasurer report for next item)
[19:19:46] <zlg> maffblaster and I are in the same time zone, I can reach out to him if need be.
[19:19:49] Current subject: treasurer report, (set by prometheanfire)
[19:19:52] <robbat2> http://dev.gentoo.org/~robbat2/20170820_treasurer_report_FY2017.html
[19:19:59] LINK: http://dev.gentoo.org/~robbat2/20170820_treasurer_report_FY2017.html [Gentoo Treasurer's report, FY2017]
[19:20:42] <robbat2> we very recently gained a CPA
[19:20:53] <prometheanfire> and it seems to have been going well
[19:20:54] <robbat2> actually as part of the FY2018, but it's still during my term in office
[19:21:10] <zlg> Right, fiscal year starting in March/April, right?
[19:21:15] <robbat2> july 1st
[19:21:18] <zlg> ah
[19:21:40] <robbat2> for the first time in many years, we have regular access to ALL of our bank accounts
[19:21:51] <zlg> \o/
[19:22:01] <robbat2> the legacy bank account with CapitalOne (that came from ING) needs to be closed down
[19:22:12] <robbat2> but we are waiting for the bank backoffice to get old statements for us
[19:22:31] <robbat2> we have to close it because they can't transfer it from tsunam to the newer trustees
[19:22:51] <dwfreed> oh hey, I almost missed the meeting
[19:22:52] <robbat2> we have withdrawn most of the balance from it, just left enough to keep it active without fees at this time
[19:23:27] <zlg> So once we have those old statements, the account can be closed?
[19:23:33] <robbat2> wyes
[19:23:34] <robbat2> *yes
[19:23:37] <zlg> sgtm
[19:23:46] <prometheanfire> very good :D
[19:23:47] <dabbott> still waiting for the address change to happen so we can get paper checks
[19:24:15] <kensington> robbat2: thank you so much for all your work on this 
[19:24:17] <dabbott> I can get checks but they will be mailed to W. Chew our agent in NM
[19:24:38] <dabbott> he is not reliable
[19:24:48] <prometheanfire> ya, the amount of work needed for all this that robbat2 has done can not be understated
[19:25:00] <robbat2> the foundation has approximately $130k between our bank accounts & paypal
[19:25:13] <shentino> eww, paypal
[19:25:30] <dabbott> we have to have a registered agent in NM for the corporation
[19:25:31] <robbat2> shentino, as a non-trustee, could you please hold your comments & questions until the open floor?
[19:25:41] <shentino> sure, sorry about that
[19:26:25] <robbat2> that covers most of the bank side
[19:26:29] <zlg> robbat2: I see total hosting expenses for FY2017 were around $4500. Is it safe to assume that, for now, the Foundation's assets are looking decent?
[19:26:35] <prometheanfire> cool, 
[19:26:46] <prometheanfire> and the bank side needs to be done before the tax side can be
[19:26:54] INFO: motion accept Treasurer's report
[19:26:59] <robbat2> i'm not done yet ;-)
[19:27:25] <robbat2> on the Taxes side, the new CPA has spoken to the IRS and figured out the rough process for us to take
[19:27:42] <robbat2> 1. verifying our actual prior filing status, pending responses from IRS already
[19:28:03] <robbat2> 2. filing a new form 1024 (either as never-before filed, or to resurrect our status for non-filing)
[19:28:26] <robbat2> 2.1. form 1024 requires filing the many outstanding form 990's (needs the bank & accounting done)
[19:29:13] <robbat2> the first of those 990's is in the private repo, in a draft state
[19:29:35] <robbat2> it needs some minor financial changes (stemming from depreciation amounts), and LOTS of non-financial answers
[19:29:59] <robbat2> of the new slate of trustees, somebody will need to take on that project
[19:30:14] <robbat2> to do all the non-financial parts of the 1024 filing
[19:30:31] <zlg> robbat2: What sort of answers would we be looking for?
[19:30:56] <dabbott> can you convert the info to a short form?
[19:31:05] <dabbott> 990ez
[19:31:09] <robbat2> i point you to Apache's filing for the same period: https://www.apache.org/foundation/records/990-2005.pdf
[19:31:20] <robbat2> no, because we did not submit it in a timely manner, we MUST do the long form
[19:31:37] <robbat2> apache's filing is as a 501(c)(3), but most of the questions are the same
[19:32:01] <robbat2> the Linux Foundation is a 501(c)(6) and their filing is also available for reading, but not as clean as the apache filing
[19:32:33] <zlg> yeah, this form looks pretty clean
[19:33:24] <robbat2> the CPA says they are not suited to helping answer the non-financial questions in the 990 & 1024
[19:33:41] <robbat2> and we either need to do it ourselves, or find counsel to help
[19:34:01] <zlg> which page of the PDF are these non-financial questions? I'm seeing 44 pages total
[19:34:46] <robbat2> it starts around page 4, but is interspersed with other financial questions
[19:34:53] <robbat2> so I have one last thing to note
[19:34:55] <zlg> Oh, okay.
[19:35:07] <zlg> assumed it was lumped together.
[19:35:24] <robbat2> as i'm an outgoing trustee, i'm stepping down from the role of treasurer, and also the infra liasion officer
[19:35:29] <dabbott> looks like yes/no answers mostly
[19:35:41] <robbat2> i have nominated jmbsvicetto as the new infra liasion officer
[19:35:44] <robbat2> and zlg as the new treasurer
[19:35:55] <prometheanfire> robbat2: what are the effective dates for both?
[19:35:56] <robbat2> and if the board will have me, I will continue as the asst. treasurer to train them
[19:36:04] <robbat2> this meeting
[19:36:36] <prometheanfire> is there anything preventing us from keeping you on in an officer role to help train them?
[19:36:46] <robbat2> the asst.treasurer is an officer
[19:37:35] <prometheanfire> I'll update the wiki for that
[19:37:37] <robbat2> i see the "end" of my process there if the new treasurer can do the month-to-month accounting, and we have no outstanding 990's
[19:37:48] <robbat2> prometheanfire: i think we should have a motion for the appointment changes
[19:38:09] <robbat2> (3 motions, one for each appointment)
[19:38:14] <robbat2> as is required in bylaws
[19:38:23] <prometheanfire> good point
[19:38:29] <prometheanfire> nothing else in your report then?
[19:38:35] <robbat2> that covers it
[19:38:50] <robbat2> motion: appoint jmbsvicetto as new infra liasion officer
[19:38:59] <dabbott> yes
[19:39:02] <prometheanfire> yes
[19:39:05] <kensington> Yes
[19:39:05] <zlg> yes
[19:39:08] <alicef> yes
[19:39:12] <prometheanfire> motion passes
[19:39:13] <robbat2> yes
[19:39:25] <robbat2> motion: appoint zlg as new treasurer
[19:39:27] <robbat2> aye
[19:39:29] <prometheanfire> yes
[19:39:33] <kensington> Yes
[19:39:33] <dabbott> yes
[19:39:34] <alicef> aye
[19:39:56] <prometheanfire> zlg: you still get a vote :P
[19:40:05] <zlg> Oh, I assumed I'd need to recuse myself.
[19:40:06] <robbat2> (he can abstain as well)
[19:40:30] <zlg> yes
[19:40:32] <veremitz> prometheanfire: you may wish to note via the meetbot too, fyi :)
[19:40:32] <prometheanfire> it's your choice
[19:40:40] <prometheanfire> motion passes
[19:41:01] <robbat2> motion: appoint robbat2 as assistant treasurer to train new treasurer & infra liasion officer
[19:41:04] <robbat2> (abstain)
[19:41:06] <prometheanfire> voting via the meetbot has never worked well for me
[19:41:10] <prometheanfire> yes
[19:41:11] <zlg> yes
[19:41:12] <kensington> Yes
[19:41:12] <alicef> yes
[19:41:14] <dabbott> yes
[19:41:18] <prometheanfire> motion passes
[19:41:23] <veremitz> prometheanfire: ack :)
[19:41:24] <robbat2> thank you
[19:41:38] <robbat2> i believe that covers everything that fell to me
[19:41:47] <prometheanfire> robbat2: I don't know why you are thanking us, you just agreed to continue with more work :P
[19:41:47] <dabbott> and motion accept treasure's report
[19:41:47] <zlg> Thank *you* for taking the trouble to train me. :)
[19:41:55] <prometheanfire> yes
[19:41:59] <robbat2> aye
[19:42:00] <kensington> Yes
[19:42:01] <zlg> yes
[19:42:16] <alicef> yes
[19:42:27] <prometheanfire> dabbott: voting?
[19:42:30] INFO: appoint jmbsvicetto as new infra liasion officer passed
[19:42:47] INFO: motion: appoint zlg as new treasure passed
[19:43:06] INFO: motion: appoint robbat2 as assistant treasurer passed
[19:43:32] INFO: motion accept treasure's report passed
[19:43:47] <dabbott> yes
[19:43:51] <robbat2> ok, so we just have the president's report left of core AGM business?
[19:43:56] <prometheanfire> motion passes
[19:43:57] <robbat2> (i have to leave shortly)
[19:44:01] <dabbott> correct
[19:44:03] <prometheanfire> robbat2: yep
[19:44:18] <prometheanfire> and it's fairly short right now and badly formatted :D
[19:44:38] <zlg> gotta start somewhere :)
[19:44:44] <prometheanfire> yep
[19:44:48] <prometheanfire> getting the link now
[19:45:13] <dabbott> prometheanfire: this is your first one no copy / paste
[19:45:31] <prometheanfire> https://dev.gentoo.org/~prometheanfire/reports/pres-2017-agm.txt
[19:45:39] <prometheanfire> yep
[19:45:47] LINK: https://dev.gentoo.org/~prometheanfire/reports/pres-2017-agm.txt [None]
[19:46:08] <robbat2> motion to approve the president's report
[19:46:12] <zlg> Re: insurance: is that $200+ a month?
[19:46:21] <prometheanfire> I plan on cleaning that up a bit and putting it in the wiki like the 2015 report https://wiki.gentoo.org/wiki/Foundation:2015_Presidents_Report
[19:46:28] <prometheanfire> zlg: 500+ a month
[19:46:32] <zlg> ouch.
[19:46:36] <prometheanfire> each
[19:46:47] <zlg> Yeah... that seems a bit high.
[19:46:49] <prometheanfire> well, maybe not that much, but close
[19:46:59] <prometheanfire> yes
[19:47:17] <robbat2> sorry, i gotta go now
[19:47:27] <prometheanfire> robbat2 made a motion
[19:47:28] <robbat2> i vote to approve that report, and the new member
[19:47:33] <prometheanfire> robbat2: thanks
[19:47:34] <zlg> robbat2: Thanks for attending, we'll be in touch soon. :)
[19:47:46] <dabbott> thanks robbat2 
[19:47:52] <prometheanfire> zlg: alicef dabbott vote on the pres report?
[19:47:55] <alicef> thanks
[19:47:58] <dabbott> yes
[19:47:58] <zlg> yes
[19:48:02] <kensington> Yes
[19:48:04] <alicef> yes
[19:48:18] <prometheanfire> cool
[19:48:21] <prometheanfire> motion approved
[19:48:21] INFO: motion to accept Presidents report passed
[19:48:43] Current subject: membership applications, (set by prometheanfire)
[19:48:47] <prometheanfire>      Marek Szuba (marecki)
[19:49:05] <prometheanfire> motion to accept marecki as a foundation member
[19:49:07] <prometheanfire> yes
[19:49:08] <alicef> aye
[19:49:10] <dabbott> yes
[19:49:12] <kensington> Yes
[19:49:13] <zlg> yes
[19:49:19] <prometheanfire> robbat2 voted yes
[19:49:23] <prometheanfire> motion passes
[19:49:30] INFO: motion to accept marecki as a foundation member passed
[19:49:31] INFO: motion to accept marecki as a foundation member passed
[19:49:37] <prometheanfire> beat ya :P
[19:49:44] <prometheanfire> ok, anything else before open floor?
[19:49:51] <dwfreed> fwiw, by bylaws, devs are automatic approval at meeting unless a trustee raises an objection
[19:50:09] <prometheanfire> true, I suppose I was really asking for that
[19:50:11] <alicef> spi 
[19:50:14] <dwfreed> whereas community members need a vote
[19:50:22] <prometheanfire> spi?
[19:50:26] <zlg> I can't think of anything at the moment. I think I'd like to learn more about my duties as treasurer before coming up with motions.
[19:50:43] <prometheanfire> alicef: oh, you have news?
[19:50:49] <alicef> robbat and I are working with the spi
[19:51:10] <alicef> they replayed to our mail trustees is in cc
[19:51:20] <zlg> checks.
[19:51:37] <alicef> Jul 26
[19:52:08] <prometheanfire> yep, I remember that
[19:52:15] <prometheanfire> zlg: I'll forward it if you want
[19:52:16] <zlg> That's before we were elected so I don't have a copy. What were you and robbat2 looking into regarding SPI?
[19:52:21] <zlg> prometheanfire: yes, please.
[19:52:57] <alicef> in summary they had similar problems to us
[19:53:08] <zlg> alicef: Lack of manpower?
[19:53:59] <dabbott> If we have an active board we are better off  having control of our assets and paying a CPA for taxes etc
[19:54:02] <alicef> I think they have similar manpower
[19:54:44] <zlg> dabbott: I'm in agreement. It may be convenient to outsource the people side of Gentoo but long-term, being independent is best.
[19:54:53] <zlg> Still, nothing wrong with inquiring.
[19:55:17] <prometheanfire> zlg: ok, forwarded the 4 emails
[19:55:23] <zlg> prometheanfire: thanks
[19:55:32] <prometheanfire> zlg@ right?
[19:55:37] <zlg> yeah
[19:55:55] <prometheanfire> ok, anything else?
[19:56:17] Current subject: date of next meeting, (set by prometheanfire)
[19:56:19] <alicef> no the other is for the open discussion
[19:56:20] <dabbott> need to update the Sept agenda with the new trustees
[19:56:26] <prometheanfire> Date of Next Meeting - Sunday, September 17 2017 19:00 UTC
[19:56:34] <dabbott> fine here
[19:56:41] <prometheanfire> that's the current date, but I suspect it will change
[19:56:45] <prometheanfire> at least the time
[19:56:49] <prometheanfire> it's fine here as well
[19:57:10] <zlg> The date works for me; I've already put a time-off request in at work.
[19:57:11] <prometheanfire> we can discuss outside the meeting a better sync up time if needed
[19:57:22] <alicef> ok for me
[19:57:26] <kensington> It's very early for me but doable
[19:57:26] <zlg> Time doesn't matter, I'm flexible.
[19:57:35] <prometheanfire> ok, tentative yes then
[19:57:44] Current subject: cleanup before open floor, (set by prometheanfire)
[19:57:53] <prometheanfire>      Who will post the log? Minutes? (dabbott )
[19:57:54] <prometheanfire>     Who will update the motions page? (dabbott )
[19:57:54] <prometheanfire>     Who will send emails? (dabbott )
[19:57:54] <prometheanfire>     Who will update agenda? (prometheanfire )
[19:57:54] <prometheanfire>     Who will update channel topic? (prometheanfire )
[19:58:02] <prometheanfire> dabbott: all good?
[19:58:23] <dabbott> yes log and minutes and motions + 1 email
[19:58:31] Current subject: open floor, (set by prometheanfire)
[19:58:34] <NeddySeagoon> raises a hand
[19:58:47] <prometheanfire> NeddySeagoon: open floor :D
[19:58:50] <NeddySeagoon> I would like to propose a vote of thank to the outgoing officers and trustees.
[19:59:28] <NeddySeagoon> and wish the nem trustees and officers good luck.
[19:59:28] <zlg> Absolutely. We wouldn't be on a path to "squared away"ness without robbat2 and the others.
[19:59:29] <alicef> thanks 
[19:59:40] <zlg> Thank you.
[19:59:51] <alicef> for all the work done
[20:00:13] <alicef> for open floor the buy of the pop banner is going on
[20:00:32] <alicef> prometheanfire: I can still send you the mail for the table ?
[20:00:38] <zlg> alicef: Do you have a picture of it?
[20:00:54] <prometheanfire> alicef: me?  I'd have thought it'd be in the bug
[20:01:02] <prometheanfire> NeddySeagoon: sgtm :D
[20:01:11] <dabbott> alicef: use the bug
[20:01:27] <alicef> zlg: you can find everything here https://github.com/gentoojp/booth-material
[20:01:33] <alicef> oh ok 
[20:01:35] <veremitz> seconds Neddy's proposal
[20:01:38] <alicef> I will use the bug
[20:01:43] <alicef> but everything is on github
[20:01:44] <alicef> https://github.com/gentoojp/booth-material
[20:02:23] <dabbott> alicef: looks great :)
[20:02:42] <prometheanfire> alicef: just add the link to the bug url field
[20:02:45] <alicef> thanks
[20:02:47] <alicef> ok
[20:02:59] <shentino> I vote yes also to thank the outgoers
[20:03:09] <zlg> alicef: Thanks, I'll bookmark it.
[20:03:22] <shentino> I would also like to question the foundation's usage of paypal.
[20:03:31] <zlg> shentino: What concerns you most?
[20:03:38] <prometheanfire> I vote yes as well
[20:03:52] <shentino> It has a bit of a bad reputation and I've read a few cases of paypal improperly freezing accounts of its users
[20:03:58] <alicef> yes
[20:04:08] <shentino> I don't want the foundation to be subject to that
[20:04:28] <shentino> so I would like to propose that the foundation, with all reasonable speed, cease and desist all usage of paypal
[20:04:32] <kensington> Do we leave funds sitting in it?
[20:04:36] <NeddySeagoon> shentino: The foundation minimises the funds held in paypal.
[20:04:50] <zlg> Yes, that is concerning. Perhaps it would be better to diversify our funding avenues rather than just ditch Paypal entirely. According to an e-mail, we use Paypal for reimbursements.
[20:04:51] <prometheanfire> shentino: I think for now it's a bit of a necessary evil, since so many thing integrate with it.  If we wish to move off of it it'd need to be a motion and we'd also have to have our bank situation fully fixed
[20:05:01] <shentino> Fair enough
[20:05:09] <prometheanfire> my understanding is that we don't leave funds sitting in it
[20:05:14] <zlg> Speaking of that, have we considered cryptocurrencies?
[20:05:21] <shentino> I just have concerns, and as an open source project I'm not sure it does our reputation any good to be dealing with paypal.
[20:05:29] <shentino> you mean like bitcoin?
[20:05:37] <zlg> bitcoin, ethereum, etc.
[20:06:04] <shentino> I advise against it.  As some may already know from recent slashdot articles there's some issues.  Block forking among them, plus all the scandals aroudn the exchanges.  My personal opinion is that it's a legal minefield
[20:06:07] <NeddySeagoon> zlg: Its been discussed in the past
[20:06:59] <prometheanfire> at the moment I don't think it's worth it, it's also reguated as a security, which complicates taxes a little
[20:07:00] <dabbott> we have to have a paypal account for some vendors to deposit sales into "cafepress" and donations, we don't hold any funds in paypal for very long
[20:07:02] <zlg> NeddySeagoon: I'll check minutes and logs.
[20:07:14] <prometheanfire> dabbott: yarp
[20:07:49] <dabbott> we also use paypal to pay some vendors, that is all they accept
[20:07:59] <prometheanfire> dabbott: are you updating the activity tracker or should I?
[20:08:25] <veremitz> hrm that's not ideal .. are there other equivalent vendors that accept checks or bank transfer?
[20:08:29] <dabbott> so to say stop using paypal is not doable at this time
[20:08:53] <zlg> shentino: In short it seems Paypal is a useful proxy and is being used as it should. I'd be open to considering more diverse payment methods, assuming it works well with our tax and/or organizational status as a non-profit.
[20:09:07] <alicef> so we can donate with cryptocurrency?
[20:09:33] <NeddySeagoon> alicef: Not today
[20:09:37] <mrueg> bug 476718
[20:09:39] <willikins> mrueg: https://bugs.gentoo.org/476718 "Request for bitcoin donation support"; Gentoo Foundation, Proposals; CONF; rich0:trustees
[20:09:40] <veremitz> crypto usually needs a reputable payment processor
[20:09:57] <zlg> mrueg: thanks for the link!
[20:10:06] <veremitz> its not impossible .. but some safeguards need to be met
[20:10:23] <shentino> So I assume we're making good progress getting our financial records cleaned up?
[20:10:35] <prometheanfire> yes
[20:10:49] <zlg> shentino: Yes, robbat2's been working on it for months, and he will be training me so I can get up to speed.
[20:10:55] <shentino> awesome!
[20:10:56] <prometheanfire> years
[20:11:07] <shentino> I look forward to the day I can donate to the foundation and claim it as a deduction on my tax return
[20:11:21] <zlg> (sorry, longer than I thought!) I already have ledger-cli experience (with my personal finances), so it seems like most of our work is the waiting game
[20:11:26] <prometheanfire> come to us with a complete proposal and we can discuss and vote on it, I don't think there are any hard NOs yet
[20:11:40] <prometheanfire> anything else before I close the meeting (we are over)?
[20:11:56] <zlg> I have nothing.
[20:12:05] <dabbott> nothing here
[20:12:23] Meeting ended by prometheanfire, total meeting length 4274 seconds
