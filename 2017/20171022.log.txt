[19:00:12] Meeting started by prometheanfire
[19:00:24] Meeting chairs are: zlg, alicef, dabbott, kensington, prometheanfire, 
[19:00:35] Current subject: roll call, (set by prometheanfire)
[19:00:38] <prometheanfire> o/
[19:00:59] <zlg> o/
[19:01:01] <alicef> o/
[19:01:20] <prometheanfire> dabbott: kensington?
[19:02:06] <zlg> (btw it's paragraph 7 that talks about stretch's gnupg using elliptical curves)
[19:03:01] <prometheanfire> they use gnutls, not openssl though (for gnupg)
[19:03:18] <zlg> ah
[19:03:26] <prometheanfire> anyway, guess I'm logging then?
[19:03:40] <prometheanfire> robbat2: jmbsvicetto ping as well
[19:04:03] <zlg> fairly sure I log this channel as well, in case anyone needs a backup.
[19:04:18] <robbat2> pong
[19:04:18] Current subject: Has the NM filing been updated? (prometheanfire), (set by prometheanfire)
[19:04:21] <prometheanfire> yes
[19:04:37] Current subject: https://wiki.gentoo.org/wiki/Foundation:Activity_Tracker, (set by prometheanfire)
[19:05:03] <prometheanfire>  Annual Report - New Mexico is scheduled for november
[19:05:12] <prometheanfire> zlg: robbat2 thats you I think
[19:05:19] <zlg> Yep, looks like us.
[19:05:39] <prometheanfire> k, just a ping to get on it :P
[19:05:45] <robbat2> no, it's not me
[19:05:50] <robbat2> it's been filed by dabbott in the past
[19:06:10] <prometheanfire> robbat2: k, it stated treasurer though
[19:06:17] <robbat2> 11/15/2018
[19:06:22] <prometheanfire> ?
[19:06:25] <robbat2> and it looks like it has been done already
[19:06:29] <robbat2> on the NMPRC site
[19:06:44] <kensington> Sorry guys 
[19:07:04] <zlg> Yeah, I remember us discussing the update of this record.
[19:07:04] <robbat2> Report Due Date:11/15/2018
[19:07:59] <prometheanfire> ok, then the wiki needs updating
[19:08:07] <prometheanfire> zlg: can you do that?
[19:08:17] <zlg> prometheanfire: sure, I can do that
[19:08:19] Current subject: irs status, (set by prometheanfire)
[19:08:32] <prometheanfire> zlg: robbat2 how goes it?
[19:08:34] <robbat2> sorry afk kid
[19:09:34] <dabbott> sorry was tied up
[19:09:39] <prometheanfire> I haven't seen much this last month, so don't expect anything
[19:09:41] <zlg> I've not seen anything new on it yet. I filled in some of the missing information we had for the MoneyMarket account (about 2 years worth of statements thanks to robbat2 )
[19:09:45] <prometheanfire> dabbott: k, all are here now :D
[19:09:50] <prometheanfire> dabbott: you logging?
[19:10:02] <zlg> But the filing itself I've not seen anything on.
[19:10:03] <dabbott> yes
[19:10:36] <prometheanfire> k, well, we can come back to it if robbat2 has additional info
[19:10:41] <zlg> sgtm
[19:10:55] <prometheanfire> zlg: dabbott you have any items to go over?
[19:11:17] <robbat2> there is no further progress on the filing, next step, as before, is producing our updated financial statements
[19:11:33] <prometheanfire> robbat2: ack
[19:11:42] <zlg> None at this time. I want to focus on learning the rest of what I need to know so I can catch up on financial activity.
[19:11:46] <robbat2> and i've had very little time
[19:11:51] Current subject: alicef -  Add Foundation:Consultants reference to https://www.gentoo.org/support, (set by prometheanfire)
[19:11:58] <prometheanfire> alicef: how goes that?
[19:12:02] <dabbott> prometheanfire: no
[19:12:19] <alicef> still not done
[19:12:42] <alicef> sorry i got buusy
[19:12:42] <prometheanfire> k, that item will stay on the itenerary then
[19:12:45] <prometheanfire> np
[19:12:55] <alicef> going to shanghai next week
[19:12:55] Current subject: alicef -  (non-corporate) donors / "friends" page, (set by prometheanfire)
[19:13:02] <prometheanfire> alicef: fun :D
[19:13:24] <alicef> i have to made such page ?
[19:13:25] <prometheanfire> how goes that one?
[19:13:35] <robbat2> the donors page was waiting on me to formalize the new sponsors proposal
[19:13:51] <prometheanfire> robbat2: ah, the various amounts right?
[19:13:59] <robbat2> yes
[19:14:33] <prometheanfire> robbat2: should I move that item to you?
[19:14:55] <robbat2> yes
[19:15:02] <prometheanfire> k
[19:15:12] <alicef> yee
[19:15:18] Current subject: copyright policy, (set by prometheanfire)
[19:15:30] <prometheanfire> this is about the FLA
[19:15:32] <alicef> not for fun
[19:15:43] <zlg> FLA?
[19:15:57] <prometheanfire> it was previously under my name, but I moved it under alicef's name as she has more experience there
[19:16:01] LINK: https://fsfe.org/activities/ftf/fla.en.html [Fiduciary Licence Agreement (FLA) - FSFE Legal]
[19:16:13] <alicef> you can move to me but I think I will be busy for working on it soon
[19:16:13] LINK: https://script-ed.org/archive/volume-10/issue-102-140-306/ [Issue 10:2 (140-306) – SCRIPTed]
[19:16:34] <prometheanfire> alicef: that's ok, this is a bigger thing that will likely involve research among all of us
[19:16:48] LINK: https://wiki.gentoo.org/wiki/User:Aliceinwire/CopyrightPolicy [User:Aliceinwire/CopyrightPolicy - Gentoo Wiki]
[19:16:54] <alicef> is not for fun, I will go to attend SOSP
[19:17:11] <robbat2> alicef: why do you keep saying 'not for fun' ?
[19:17:23] <alicef> is replay to prometheanfire
[19:17:25] <prometheanfire> ok, no progress yet, but it looks like the web hosting for https://contributoragreements.org/ is gone (squater)
[19:17:30] <dabbott> her trip is not for fum
[19:17:41] <prometheanfire> oh
[19:17:44] <dabbott> alicef: sneek some in :)
[19:17:48] <prometheanfire> now I get that response
[19:17:59] <prometheanfire> yep, that's the key to intl trips, sneaking fun in
[19:18:26] <alicef> they probably moved the page
[19:18:42] <alicef> prometheanfire: http://contributoragreements.org/
[19:19:00] <alicef> someone added https
[19:19:11] <prometheanfire> with the wrong cert
[19:19:12] <prometheanfire> ya
[19:19:27] <prometheanfire> ok, moving on
[19:19:46] Current subject: prometheanfire -  join the call for Public Code https://publiccode.eu (done), (set by prometheanfire)
[19:19:52] <alicef> prometheanfire: no to the wiki link 
[19:20:04] <prometheanfire> alicef: that was probably me
[19:20:15] <alicef> this is finished
[19:20:23] <alicef> +1 prometheanfire
[19:20:25] <prometheanfire> yep
[19:20:32] <prometheanfire> next, the fun items
[19:20:40] <alicef> \o/
[19:20:48] Current subject: larrythecow.org , (set by prometheanfire)
[19:20:58] LINK: https://bugs.gentoo.org/634406 [634406 – larrythecow.org potentially(?) profiting off of Gentoo mascot's name.]
[19:21:06] <robbat2> beandog used to own the domain, but he let it lapsed, present owner unknown
[19:21:10] <robbat2> *let it lapse
[19:21:39] <zlg> looks like it's a laptop review blog now.
[19:21:48] <prometheanfire> I think we should reach out to them (the current owner), specifically about the use of our copyright
[19:21:57] <alicef> i see the gentoo logo there and looks like is only suggesting to buy free laptop
[19:21:59] <dabbott> a funtoo user gave it to drobbins (Funtoo) at one point I think
[19:22:16] <prometheanfire> point them to our logo usage guidelines
[19:22:37] <prometheanfire> I'm not sure we can get them to stop using the domain itself, but should be able to get them to stop using the logo
[19:22:40] <zlg> After reading a few pages, it's all the same marketing-speak stuff.
[19:22:48] <prometheanfire> zlg: yep
[19:22:52] <zlg> Yeah, the logo is clearly in violation.
[19:22:55] <alicef> mmm
[19:23:02] <alicef> ok 
[19:23:04] <zlg> I had asked prior if we have any standard copy to send violators.
[19:23:17] <zlg> Or if each of our letters are written from scratch.
[19:23:19] <prometheanfire> zlg: I don't think we do
[19:23:37] <zlg> If I know the requirements of the copy, I can draft something up.
[19:23:38] <prometheanfire> I'm going to start out as a normal email to inform them
[19:23:41] <dabbott> I never saw anything
[19:23:56] <prometheanfire> if that doesn't work we can make it 'official'
[19:25:11] <prometheanfire> I'd like to vote on that plan of action
[19:25:22] <zlg> I'm in favor of reaching out. It's a clear violation.
[19:25:25] <alicef> Registry Expiry Date: 2018-10-28T23:11:06Z 
[19:25:35] <alicef> Registrar URL: http://www.name.com 
[19:26:04] <robbat2> i think there might be a letter we had written in the past, but it would probably require substantial updates, so better to start fresh
[19:26:09] <prometheanfire> email them to inform of logo usage guidelines (to get them to stop using our logo), failing that, contact hosting provider
[19:26:14] <prometheanfire> vote please ^
[19:26:16] <prometheanfire> aye
[19:26:16] <zlg> yes
[19:26:24] <kensington> Yes
[19:26:36] <dabbott> yes
[19:26:44] <alicef> yes
[19:26:47] <prometheanfire> k
[19:26:58] <prometheanfire> I'll send a draft to the trustees alias first
[19:27:08] <dabbott> prometheanfire: thanks
[19:27:11] <prometheanfire> next
[19:27:26] Current subject: openssl ecc patents issue, (set by prometheanfire)
[19:27:36] LINK: https://bugs.gentoo.org/531540 [531540 – dev-libs/openssl: revise inclusion of elliptic curves with bindist USE flag]
[19:27:45] <zlg> A quick search indicates there's a lot of ECC patents.
[19:28:00] <prometheanfire> robbat2: I believe you've done some work there as well
[19:28:02] <robbat2> at the licenses team, ulm & I have agreed that Fedora's approach is the safest one at this time, and ulm has approved the patch I used
[19:28:25] <robbat2> it's just Fedora's patch for 1.1.0, applied on top of the 1.1.0 ebuild
[19:28:33] <robbat2> the catch herein, is that we still have 1.1.0 is package.mask
[19:28:49] <robbat2> and Fedora doesn't have a current version of their patch for openssl 1.0
[19:28:53] <prometheanfire> robbat2: that's only allowing specific curves by default?
[19:28:56] <robbat2> yes
[19:29:08] <prometheanfire> robbat2: have we searched their git history?
[19:29:17] <robbat2> yes, I can link you to the earlier patches
[19:29:24] <robbat2> so what it does:
[19:29:33] <robbat2> - remove any curves that are covered by patents
[19:29:41] <robbat2> - remove any EC methods that are covered by patents
[19:30:05] <robbat2> - disable/modify anything that linked/referenced those
[19:30:36] <robbat2> the methods part worries me more than the curves
[19:30:41] <robbat2> because the curves are well-contained in the codebase
[19:30:52] <prometheanfire> will users be able to opt out of that disabling?
[19:31:10] <robbat2> USE=-bindist, as presently, gets you the FULL openssl
[19:31:19] <robbat2> USE=bindist will give you the limited subset of openssl
[19:31:51] <prometheanfire> k
[19:32:20] <zlg> What will this patch mean for releng? Can they ship dev-python/cryptography with USE=bindist on openssl?
[19:32:21] <prometheanfire> by your ommision it sounds like the methods are not well contained in the code base?
[19:32:39] <prometheanfire> zlg: I think so
[19:33:02] <robbat2> dwfreed was going to test it, but it should build, at worst with some patches to ensure it doesn't try disabled curves
[19:33:21] <dwfreed> I don't remember saying I was going to test it
[19:33:26] <zlg> The patch sounds good, and other larger distros appear to have avoided legal trouble with it.
[19:33:28] <prometheanfire> at the least, fedora should have those patches
[19:33:42] <prometheanfire> if any are needed
[19:33:58] <zlg> maybe we could reach out to them and ask for details regarding the decision(s).
[19:34:06] <zlg> I doubt they'd make that decision lightly.
[19:34:12] <robbat2> prometheanfire: yes, the methods aren't well-contained, that's why USE=bindist passes disable-ecm2
[19:34:15] <robbat2> *ec2m
[19:34:35] <prometheanfire> k
[19:34:57] <robbat2> i trust the Fedora person behind the decision, I know from prior experience he makes the choices for all of Redhat, with the backing of RH legal
[19:35:19] <prometheanfire> well, I'm in favor of using the patches, with a directive to try and getting it working with openssl-1.0 if possible
[19:35:38] <robbat2> https://lwn.net/Articles/714524/
[19:35:52] LINK: https://lwn.net/Articles/714524/ [This is why I drink: a discussion of Fedora's legal state [LWN.net]]
[19:35:53] <robbat2> "Elliptic curve cryptography is now in Fedora, after a six-year wait for the base functionality, and a ten-year wait for the curves currently used. "
[19:36:06] <prometheanfire> yep, that was a good article
[19:36:11] <zlg> Looks like a good read
[19:36:28] <prometheanfire> k, I think we should vote
[19:36:40] <robbat2> the options on the table for it
[19:36:49] <robbat2> 1. keep USE=bindist very safe, no EC at all
[19:36:58] <robbat2> 2. use the Fedora patches, hobbled-EC
[19:37:10] <robbat2> 3. talk to Debian, and take their full EC approach
[19:37:19] <robbat2> did I miss a possible outcome?
[19:37:30] <prometheanfire> using the patches (as fedora does, aka, the one ported by robbat2 from fedora), with a directive to try and getting it working with openssl-1.0 as well, if possible
[19:37:34] <prometheanfire> nope
[19:37:37] <zlg> robbat2: lgtm
[19:38:01] <dwfreed> 1 is not an option if you want mirrorselect on min-install CDs, just fyi
[19:38:04] <prometheanfire> debian would be easier, does anyone know anyone over there?
[19:38:26] <prometheanfire> dwfreed: yep, I'm in favor of option 3 if we can, but at least option 2
[19:38:30] <robbat2> rewrite mirrorselect ;-)
[19:38:40] <dwfreed> this is the rewrite
[19:38:47] <prometheanfire> re-rewrite it :D
[19:39:21] <prometheanfire> if it's in python cryptography is THE library to use
[19:39:25] <zlg> I've had limited communication with Debian. Their technical committee is probably the point of contact.
[19:39:36] <dwfreed> convince cryptography to revert making EC non-optional
[19:39:45] <prometheanfire> dwfreed: good luch
[19:39:47] <prometheanfire> luck
[19:39:53] <dwfreed> that'd be the only way you could have 1 and mirrorselect on min-install
[19:40:02] <zlg> dwfreed: How do they legally get away with that?
[19:40:07] <zlg> If they write libre software...
[19:40:24] <prometheanfire> it's a requirement, they don't make the decision to use themselves
[19:40:28] <prometheanfire> downstreams do
[19:41:00] <dwfreed> it's mostly a sane wrapper around openssl and/or other crypto libraries
[19:41:26] <prometheanfire> robbat2: how hard would it be to get it into 1.1? can we do that while at the same time talking to deb?
[19:41:37] <dwfreed> it's mostly python, with a C component for the interface to crypto libraries
[19:41:41] <zlg> The patch is for 1.1 afaik
[19:41:48] <robbat2> you mean 1.0; it's just a matter of some dev time, which I'm short on
[19:41:51] <prometheanfire> it is
[19:42:06] <robbat2> the 1.1 patches just-worked, the 1.0 didn't
[19:42:17] <prometheanfire> robbat2: I mean to get the patch for 1.1 in tree, while we contact debian for more info
[19:42:26] <prometheanfire> that would force releng to use 1.1 though
[19:42:33] <robbat2> that wouldn't be bad
[19:42:46] <robbat2> i think it's probably doable
[19:42:53] <robbat2> since Fedora is shipping 1.1 as default
[19:42:56] <prometheanfire> even with 1.1 being masked?
[19:42:59] <robbat2> and ubuntu does as of last week too
[19:43:30] <zlg> patch + talk to debian + try to backport for 1.0 sounds like the best course of action.
[19:43:53] <prometheanfire> well, if we can unmask 1.1 and start the stable process that sounds good too (instead of backporting)
[19:44:02] <prometheanfire> so, to vote
[19:44:04] <robbat2> it has been in package.mask for a long time
[19:44:34] <prometheanfire> patch 1.1, talk to debian, (try to backport for 1.0 OR work on getting 1.1 stable)
[19:44:42] <zlg> yes
[19:44:47] <prometheanfire> yes
[19:44:56] <dabbott> yes
[19:45:14] <kensington> Abstain 
[19:45:16] <zlg> (side note: where is releng? it'd be nice to hear from them)
[19:45:48] <prometheanfire> alicef: ?
[19:45:50] <alicef> abstein
[19:45:52] <prometheanfire> k
[19:45:58] <robbat2> jmbsvicetto has been busy with IRL stuff, so I'm the closest you have to releng here
[19:46:03] <robbat2> as the releng-infra liason
[19:46:11] <prometheanfire> we have quorum
[19:46:15] <zlg> oh yeah, I recall you mentioning that
[19:46:15] <alicef> Abstain
[19:46:21] <alicef> xd
[19:46:47] <prometheanfire> robbat2: you want to head that up? (I'll make a comment on the bug)
[19:47:06] <zlg> I can reach out to Debian if robbat2 's too busy.
[19:47:18] <robbat2> i will put my 1.1 in the tree
[19:47:20] <zlg> most of the patch work appears to be done already.
[19:47:22] <robbat2> somebody else gets the rest
[19:47:24] <prometheanfire> zlg: k
[19:47:38] <robbat2> i will link the existing Fedora 1.0 patches as well
[19:47:46] <prometheanfire> robbat2: ok, so it's you and zlg I think
[19:47:57] <prometheanfire> next item
[19:48:04] <zlg> robbat2: let me know what you can't get to via e-mail or IRC, I'll try to figure it out from there.
[19:48:07] Current subject: infra update, (set by prometheanfire)
[19:48:14] <prometheanfire> robbat2: that's you :D
[19:48:19] <prometheanfire> how are the new drives/server?
[19:48:24] <zlg> oh yeah, the new storage! :D
[19:48:34] <alicef> \o/
[19:48:41] <robbat2> the parts are in place as of this past Monday
[19:48:56] <robbat2> i did some quick benchmarks to test the storage, but haven't set up MySQL on them yet
[19:48:59] <robbat2> ENOTIME
[19:49:15] <prometheanfire> ok, any other infra updates?
[19:49:52] <robbat2> nothing else comes to mind right now
[19:49:54] <prometheanfire> k
[19:50:05] <zlg> is that it for agenda items?
[19:50:10] Current subject: Treasurer update, (set by prometheanfire)
[19:50:15] <prometheanfire> zlg: that's you :D
[19:50:30] <prometheanfire> next is bugs, then that's it
[19:50:36] <zlg> robbat2 and I worked together to catch up the MoneyMarket account from 2013-12 to 2015-12.
[19:50:50] <zlg> So there's a good swath of statements that are known-good now.
[19:51:20] <zlg> robbat2 can correct me if I'm wrong but I think next is to get caught up on recent transactions.
[19:51:33] <robbat2> and the error margin was only a few cents in the wrong months :-)
[19:51:40] <prometheanfire> robbat2: very good :D
[19:51:46] <robbat2> two seperate parts for next-steps
[19:51:51] <zlg> Indeed! it all balanced out well, it was a matter of where the pennies were going.
[19:52:04] <robbat2> as before, 1. financial statements of older data for IRS needs
[19:52:16] <robbat2> 2. recent transaction importing as data becomes available
[19:52:42] <prometheanfire> nice
[19:52:43] <dabbott> Is the accountant still on board?
[19:52:47] <zlg> I don't think I have access to all that I'd need to fetch all the data.
[19:52:58] <zlg> e.g. csv and statements from Paypal, et al
[19:53:16] <robbat2> zlg: then you haven't looked closely enough in the repos
[19:53:24] <zlg> fair enough :)
[19:53:24] <robbat2> the passwords for Paypal ARE there
[19:53:32] <robbat2> in the encrypted files
[19:53:48] <robbat2> the accountant is still available, but is also waiting for us to do the financial statements
[19:53:53] <zlg> Ah, I had assumed I didn't have the keys to open them.
[19:54:03] <dabbott> robbat2: ok thanks
[19:54:12] <prometheanfire> k, bugs next
[19:54:14] <robbat2> there's a bunch of changes needed in data to get those financial statements, like fixing forex transactions & depreciation
[19:54:34] <zlg> depreciation, fun
[19:54:54] <prometheanfire> .subect bugs
[19:55:02] Current subject: bugs, (set by prometheanfire)
[19:55:09] LINK: https://goo.gl/CTX1qO [Bug List: TrusteesOpenBugs]
[19:55:22] <prometheanfire> we already went over 2 bugs
[19:55:28] <zlg> robbat2: is e-mail the better way to reach you since you're busy? 
[19:56:06] <robbat2> try email if you can't find me on IRC
[19:56:12] <zlg> okay, I'll remember that.
[19:56:43] <prometheanfire> I don't see any new items outside of the usual funding requests
[19:56:55] <prometheanfire> I do have a suggestion about the bugs
[19:57:09] <robbat2> adding a last-checked item in whiteboard?
[19:57:22] <prometheanfire> a flag could be created so we know if something is actionable
[19:57:25] <prometheanfire> robbat2: close :P
[19:57:33] <zlg> an actionable flag sounds good.
[19:57:41] <prometheanfire> the user would set this flag so we know to go over it
[19:57:45] <robbat2> actionable by who is the question
[19:57:47] <robbat2> consider bug 
[19:57:51] <robbat2> bug 607622
[19:57:54] <willikins> robbat2: https://bugs.gentoo.org/607622 "a new sparc machine is needed"; Gentoo Foundation, Infra Support; CONF; ago:trustees
[19:58:05] <robbat2> we are waiting for the sparc team to attach a funding proposal
[19:58:07] <prometheanfire> actionable by trustees
[19:58:46] <zlg> hmm.. good point. If the Foundation is waiting for action from another group, do we just CC them and leave the ball in their court?
[19:58:48] <robbat2> last-reviewed date would help as well, so we can clearly see when we last looked at it, or gave a direction
[19:58:56] <kensington> I think we should review rotting bugs and close them where possible 
[19:58:59] <prometheanfire> but we can discuss that offline
[19:59:02] <zlg> kensington: ++
[19:59:09] <alicef> this is spam https://bugs.gentoo.org/607622#c11 ?
[19:59:43] <prometheanfire> alicef: nope
[19:59:49] <robbat2> alicef: that's weird, it's a real comment with the spammy link
[19:59:57] <prometheanfire> yep
[20:00:23] <alicef> https://www.ultimatewebtraffic.com/ this link is completely not related with what we are talking
[20:00:38] <zlg> don't some of us have the ability to edit bug comments?
[20:00:43] <robbat2> yeah, i'm going to redact that comment for spam, and repost it without the link
[20:00:46] <robbat2> only infra does
[20:00:50] <prometheanfire> robbat2: k
[20:00:54] <alicef> is comment could be auto generated 
[20:01:01] <alicef> his
[20:01:03] <prometheanfire> https://bugs.gentoo.org/631446 can be closed I think?
[20:01:48] <zlg> We could check that user to see if they've posted the same spam elsewhere on the tracker.
[20:02:00] <kensington> If it's done sure 
[20:02:14] <robbat2> prometheanfire: only closed after that is entered in the accounting data
[20:02:24] <robbat2> expense against goodwill
[20:02:24] <alicef> check if ir usual thing for Nico Bareto  to post spam and ban in case it is
[20:02:28] <prometheanfire> robbat2: it's not being reimbursed
[20:02:43] <prometheanfire> not sure how that gets accounted
[20:02:48] <robbat2> prometheanfire: yes, that's the 'goodwill' part, you effectively donated the fee to us
[20:02:55] <prometheanfire> ah, right
[20:03:07] <prometheanfire> ok, will be open til you or zlg gets to it then
[20:03:10] <robbat2> i have to go for the kids again; lots of the open bugs are waiting for treasurer data entry :-)
[20:03:19] <zlg> robbat2: I noticed... x_x
[20:03:33] <robbat2> alicef: are you going to buy the other item in your funding request or not?
[20:03:43] <alicef> not now 
[20:03:47] <prometheanfire> yep, lets move on
[20:03:51] Current subject: open floor, (set by prometheanfire)
[20:03:56] <robbat2> alicef: ok, if you DO want it in future, please open a new funding request for it
[20:04:21] <alicef> no one helped me making the table cover so i'm dropping it for now
[20:04:22] <prometheanfire> alicef: you need the traveling mailbox password file reencrypted with your key right?
[20:04:30] <alicef> yes
[20:05:02] <prometheanfire> robbat2: I think I can do it, I just have to ecrypt the file with all of our keys (trustee/officer) right?
[20:05:30] <robbat2> prometheanfire: there's a helper comment in one of the toplevel textfiles
[20:05:34] <robbat2> that should catch all the keys
[20:05:37] <prometheanfire> k
[20:05:51] <robbat2> trustees-gpg.txt.asc
[20:06:14] <prometheanfire> date of next meeting is currently November 19th, is that fine with everyone?
[20:06:29] <kensington> Yep
[20:06:29] <zlg> I should be able to get that day off.
[20:06:37] <dabbott> fine here
[20:06:41] <robbat2> yes, Nov 19th is good with me
[20:07:02] <prometheanfire> alicef: nov 19 good for you?
[20:07:19] <alicef> ok
[20:07:35] <prometheanfire> k, anyone else have any remaining items?
[20:07:48] <zlg> I'm good.
[20:07:57] <alicef> i'm good
[20:08:17] <prometheanfire> k
[20:08:17] <prometheanfire> # Who will post the log? Minutes? ({{U|dabbott}})
[20:08:18] <prometheanfire> # Who will update the motions page? ({{U|aliceinwire}})
[20:08:18] <prometheanfire> # Who will send emails? ({{U|dabbott}})
[20:08:18] <prometheanfire> # Who will update agenda? ({{U|prometheanfire}})
[20:08:20] <prometheanfire> # Who will update channel topic? ({{U|prometheanfire}})
[20:08:29] <prometheanfire> just so people know their jobs :D
[20:08:32] <zlg> I'm gonna update our activity tracker and reach out to Debian to figure out their reasoning for ECC inclusion.
[20:08:41] Meeting ended by prometheanfire, total meeting length 4109 seconds
