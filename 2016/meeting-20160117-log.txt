Jan 17 14:00:02 *	NeddySeagoon bangs the gavel to open the January 2016 Gentoo Foundation Trustees Meeting
Jan 17 14:00:11 <SwifT>	i'm here
Jan 17 14:00:26 <dabbott>	o/
Jan 17 14:00:31 <NeddySeagoon>	Roll Call ... dabbott robbat2 antarus 
Jan 17 14:00:52 <dabbott>	here
Jan 17 14:01:06 <antarus>	aye
Jan 17 14:01:10 <robbat2>	hi
Jan 17 14:01:13 <NeddySeagoon>	Looks like a full turnout  ...   Happy New Year!
Jan 17 14:01:28 <SwifT>	best wishes for you all ;)
Jan 17 14:01:34 <NeddySeagoon>	dabbott: do you have the log ?
Jan 17 14:01:39 <dabbott>	yes
Jan 17 14:01:53 <NeddySeagoon>	Thank you
Jan 17 14:02:02 <NeddySeagoon>	Old Business ...
Jan 17 14:02:52 <NeddySeagoon>	robbat2: anything to add to the email traffic, or that we need a public record of ?  New Treasurer and account details
Jan 17 14:03:29 <robbat2>	matthew has NOT mailed me the physical stuff he had
Jan 17 14:03:32 *	ChanServ gives channel operator status to quantumsummers
Jan 17 14:03:36 <robbat2>	woh!
Jan 17 14:03:48 <quantumsummers>	Hello :-)
Jan 17 14:04:02 <NeddySeagoon>	Hi quantumsummers 
Jan 17 14:04:07 <quantumsummers>	Kids are sorta napping, so I should have a few mins before things go south
Jan 17 14:04:08 <robbat2>	quantumsummers: great, so my call wasn't for nought (i tried just before the meeting and got voicemail yet again)
Jan 17 14:04:18 <quantumsummers>	You called?
Jan 17 14:04:25 <robbat2>	quantumsummers: in order of priority, I need
Jan 17 14:04:30 <quantumsummers>	yessir.
Jan 17 14:04:35 <robbat2>	1. your SSN to fix access to the main account
Jan 17 14:04:50 <robbat2>	2. the contact details for the prior CPA/tax person you had
Jan 17 14:04:52 <quantumsummers>	the cap1-360?
Jan 17 14:05:05 <robbat2>	3. the chequebook
Jan 17 14:05:12 <robbat2>	4. the hardcopy docs you have
Jan 17 14:05:21 <robbat2>	yes the 360 account
Jan 17 14:05:33 <robbat2>	the one that didn't work, they say the ssn of the holder is needed to reset it
Jan 17 14:05:55 <quantumsummers>	ok, I can call. There is some form to transfer account to new name too.
Jan 17 14:06:05 <robbat2>	yes, but i need to access it first to get that
Jan 17 14:06:13 <robbat2>	you don't need to call
Jan 17 14:06:17 <robbat2>	there's a web page to reset it
Jan 17 14:06:19 <robbat2>	i'll link you
Jan 17 14:06:27 <quantumsummers>	I was unsuccessful the last time I tried that.
Jan 17 14:06:39 <robbat2>	with your SSN?
Jan 17 14:06:45 <quantumsummers>	Whatever the case, I get the paper statements here at my house.
Jan 17 14:06:48 <quantumsummers>	Yes, my SSN
Jan 17 14:07:01 <robbat2>	ok, then you'll have to call them (or give me the SSN to call with)
Jan 17 14:07:02 <quantumsummers>	last 4 digits anyway
Jan 17 14:07:06 <robbat2>	yep
Jan 17 14:07:21 <quantumsummers>	I will call on Tuesday, Monday is a banking holiday.
Jan 17 14:07:22 <robbat2>	on to #2 in that list then
Jan 17 14:07:33 <robbat2>	you said your tax person filed an extension
Jan 17 14:07:38 <robbat2>	that 3 months would be up this month
Jan 17 14:07:47 <robbat2>	hence, i need to get in touch with them asap
Jan 17 14:07:50 <quantumsummers>	#2 yes, I can get that info to you.
Jan 17 14:08:15 <robbat2>	#3/#4: please ship to me as soon as you can, i need to cut at least one chque already
Jan 17 14:08:20 <quantumsummers>	I should call them to see if they filed the postcard or what.
Jan 17 14:09:05 <robbat2>	i need to talk to them regardless, as I have the info they need to file
Jan 17 14:09:11 <quantumsummers>	#3 no prob. #4 I'll ship what I have on hand. I know some stuff is boxed up in storage.
Jan 17 14:09:40 <robbat2>	ship #3/#4 on monday if a shipping store is open (since it's a holiday)
Jan 17 14:09:47 <quantumsummers>	I have an address for you Robin, so I'll fedex if that works.
Jan 17 14:09:50 <robbat2>	yep
Jan 17 14:09:58 <quantumsummers>	a-ok
Jan 17 14:10:00 <robbat2>	and we'll reimburse
Jan 17 14:10:11 <quantumsummers>	eh, not going to worry about it unless it's really pricy
Jan 17 14:10:40 <quantumsummers>	I do have a question for the board.
Jan 17 14:10:43 <robbat2>	yes
Jan 17 14:11:07 <quantumsummers>	My weekends are now essentially over-run by kids. We have 3 now as you may know.
Jan 17 14:11:21 <quantumsummers>	Any chance we can move our board meeting to a weekday?
Jan 17 14:11:33 <quantumsummers>	If that was possible, I could definitely attend.
Jan 17 14:11:46 <quantumsummers>	Sundays are just very difficult 
Jan 17 14:12:04 <robbat2>	i think you missed the minutes/motions; we drafted dabbott to replace you
Jan 17 14:12:12 <quantumsummers>	If moving the meeting is not possible, which I understand...
Jan 17 14:12:18 <robbat2>	in light of your lack of time
Jan 17 14:12:22 <quantumsummers>	yes, I have not seen those notes.
Jan 17 14:12:32 <quantumsummers>	Yes, that makes sense.
Jan 17 14:12:34 <NeddySeagoon>	quantumsummers: Do you really have the time at all with 3 wee ones?
Jan 17 14:13:08 <quantumsummers>	NeddySeagoon: I can take the time, just not on the weekends.
Jan 17 14:13:22 <quantumsummers>	It's a matter of luck today.
Jan 17 14:13:40 <antarus>	From my POV I care less about the meetings and more about the difficulties getting in touch with you
Jan 17 14:13:53 <antarus>	particularly regardin the financial details and the filing of the foundations taxes
Jan 17 14:14:02 <quantumsummers>	Understandable.
Jan 17 14:14:08 <NeddySeagoon>	quantumsummers: I'm not so sure.  No treasurers reports for two years ...   You should be spending time with your family
Jan 17 14:14:39 <quantumsummers>	I won't argue the point.
Jan 17 14:15:28 <NeddySeagoon>	quantumsummers: You would be doing the foundation a service if you expidate the handover to robbat2 
Jan 17 14:15:31 <quantumsummers>	I have not done well with this work for some time now.
Jan 17 14:16:16 <quantumsummers>	NeddySeagoon: Yes, I agree. I do not wish dis-service'
Jan 17 14:16:18 <dabbott>	quantumsummers: please help us out and get the info we need
Jan 17 14:16:43 <quantumsummers>	dabbott: I will.
Jan 17 14:16:48 <dabbott>	if you can take the time to do that it would be a big help
Jan 17 14:17:19 <dabbott>	its real hard to do it without your help
Jan 17 14:17:38 <dabbott>	it can be done but why make it hard on us
Jan 17 14:17:41 <NeddySeagoon>	quantumsummers: Its not the meetings ... they are only the tip of the iceberg.  Its all the work the treasurer needs to do off line that takes time.  Don't miss your children growing up
Jan 17 14:17:44 <quantumsummers>	I can take some time as noted above. Understood, just needs to be done.
Jan 17 14:18:51 <quantumsummers>	ok, so: 1. call cap1, get the money market account situated (that is the one I cannot log into). 2. CPA 3. mail stuff 
Jan 17 14:19:18 <quantumsummers>	the cap1 360 I had no problem accessing, that is the one with the bulk of the funds
Jan 17 14:19:38 <robbat2>	yeah, but difficult to get it out from my perspective
Jan 17 14:19:57 <quantumsummers>	yes, there is no cheque book for that one
Jan 17 14:20:01 <quantumsummers>	it's a savings account
Jan 17 14:20:20 <robbat2>	it's transfer to other acct, then use chequebook
Jan 17 14:20:26 <robbat2>	which you still have ;-)
Jan 17 14:20:47 <robbat2>	and EFT payments would also be the money market acct
Jan 17 14:21:11 <quantumsummers>	yes, and direct deposits went there too
Jan 17 14:22:39 <robbat2>	quantumsummers: anyway, please fix the account on tuesday when the bank is open; and pack a box with the other stuf now, to ship
Jan 17 14:22:44 <quantumsummers>	if it were me, I would push the funds from the money market into the 360, then start a new checking with the 360 attached.
Jan 17 14:23:05 <robbat2>	i was going to make some other banking comments later
Jan 17 14:23:06 <quantumsummers>	robbat2: sure, that works.
Jan 17 14:23:14 <robbat2>	if you're done with your remarks so we can continue the meeting
Jan 17 14:23:24 <quantumsummers>	proceed, please.
Jan 17 14:23:37 <robbat2>	(under new business, i was already planning a discussion of a new, more accessible account)
Jan 17 14:23:40 <NeddySeagoon>	robbat2: anything to add to the email traffic, or that we need a public record of ? New Treasurer and account details
Jan 17 14:24:05 <robbat2>	NeddySeagoon: in your inbox soon, will be the forms to sign to fix the signers on the accounts
Jan 17 14:24:30 <NeddySeagoon>	robbat2: OK
Jan 17 14:24:49 <NeddySeagoon>	Activity Tracker
Jan 17 14:24:57 <robbat2>	no other old business from me
Jan 17 14:25:48 <NeddySeagoon>	Gentoo name trademark affirmation of use is that 2015 dat still correct?
Jan 17 14:25:59 <robbat2>	i think we filed on it
Jan 17 14:26:16 <NeddySeagoon>	The tracker needs to be updated then
Jan 17 14:26:27 <quantumsummers>	we're good on those for another several years
Jan 17 14:26:40 <NeddySeagoon>	quantumsummers: thatnk you
Jan 17 14:26:55 <dabbott>	Annual report was updated, I can remove that from the agenda
Jan 17 14:26:57 <quantumsummers>	I have a huge amount of stuff from the previous attorney, anyone want that mailed?
Jan 17 14:27:05 <NeddySeagoon>	dabbott: Thanks
Jan 17 14:27:05 <quantumsummers>	re: trademarks that is
Jan 17 14:27:16 <robbat2>	quantumsummers: yes
Jan 17 14:27:26 <quantumsummers>	robbat2: ok, will include it.
Jan 17 14:27:29 <NeddySeagoon>	quantumsummers: probably robbat2 
Jan 17 14:28:02 <NeddySeagoon>	robbat2: Treasurer handover status   ... is that covered?
Jan 17 14:28:19 <robbat2>	i have remarks in new business
Jan 17 14:28:32 <robbat2>	we covered the existing pieces it with quantumsummers turning up now :-)
Jan 17 14:28:46 <NeddySeagoon>	Financial records ... SFC donation (bug 568896)  ... OK
Jan 17 14:28:49 <willikins>	NeddySeagoon: https://bugs.gentoo.org/568896 "Funding Request: Matching Funds Donation to Software Freedom Conservancy (SFC)"; Gentoo Foundation, Proposals; CONF; robbat2:trustees
Jan 17 14:28:57 <robbat2>	SFC donation blocked on the chequebook
Jan 17 14:29:02 <quantumsummers>	shit.
Jan 17 14:29:23 <quantumsummers>	want me to write the check and mail that tomorrow as well?
Jan 17 14:29:35 <robbat2>	quantumsummers: yes, i'll give you the details in a PM
Jan 17 14:29:47 <NeddySeagoon>	Gentoo OVH Release 2  ... OVH seem to be selling less and less Gentoo installs.  I've still done nothing
Jan 17 14:30:32 <NeddySeagoon>	SwifT: Gentoo Trademark License Agreement   we were going ahead without the e.V.
Jan 17 14:30:46 <SwifT>	yes, but I haven't been able to work on that since last meeting
Jan 17 14:30:55 <SwifT>	I wanted to clarify the overlay situation in it first
Jan 17 14:31:32 <SwifT>	I gave myself an ETA for march (as I have a thesis due by end februari so very small change that i will able to doit sooner)
Jan 17 14:31:44 <SwifT>	s/change/chance/
Jan 17 14:32:33 <SwifT>	that's all I have to say on it for now (sorry)
Jan 17 14:33:08 <NeddySeagoon>	SwifT: Good luk with the thesis
Jan 17 14:33:13 <SwifT>	thx
Jan 17 14:33:16 <dabbott>	yes :)
Jan 17 14:33:35 <dabbott>	OT whats it on
Jan 17 14:33:38 <NeddySeagoon>	Bugs
Jan 17 14:34:15 <NeddySeagoon>	I've forgotten my password.  robbat2 care to ghair this section please?
Jan 17 14:34:27 <robbat2>	one moment
Jan 17 14:34:46 <NeddySeagoon>	The link just used to work :(
Jan 17 14:35:07 <SwifT>	dabbott: it's about a minimum viable, sustainable enterprise architecture for organizations that use modern delivery methods (such as agile development)
Jan 17 14:35:09 <robbat2>	locked bug #472574
Jan 17 14:35:18 <robbat2>	there is a potential new dev from iran
Jan 17 14:35:34 <robbat2>	i think we can accept them in light of the USA lifting sanctions
Jan 17 14:35:45 <dabbott>	yes, why not
Jan 17 14:35:49 <quantumsummers>	I would ask an attorney.
Jan 17 14:35:50 <NeddySeagoon>	robbat2: Sounds good
Jan 17 14:36:19 <NeddySeagoon>	quantumsummers: thats alwoys safe
Jan 17 14:36:50 <quantumsummers>	Iran is still listed as a state sponsor of terrorism by the US Dept of State
Jan 17 14:37:03 <robbat2>	locked bug #569940: fosdem DVDs; purchase is go, locked as the attached quote contains private details; final price looks like 265EUR
Jan 17 14:37:24 <NeddySeagoon>	robbat2: That has my vote
Jan 17 14:37:33 <robbat2>	yes, we already approved it last meeting
Jan 17 14:37:57 <robbat2>	no other trustees bugs I see with any progress or needing updates
Jan 17 14:38:09 <NeddySeagoon>	Thank you robbat2 
Jan 17 14:38:28 <robbat2>	would be nice to clean out old bugs something, but not during a meeting
Jan 17 14:38:29 <NeddySeagoon>	New Business
Jan 17 14:39:16 <dabbott>	We checked the meeting dates last month, I will remove from agenda
Jan 17 14:39:16 <NeddySeagoon>	Consider the proposed meetings calendar for the year ahead.   I may not be able to do June 19 any more.  I have to mind my mother for a week
Jan 17 14:39:57 <NeddySeagoon>	Trademark Considerations.   "Genfleetoo" (by email to the trustees) 
Jan 17 14:40:13 <NeddySeagoon>	Not with our logo but distorted 
Jan 17 14:40:42 <robbat2>	meeting: i cannot do june 26; how about june 12th?
Jan 17 14:40:57 <dabbott>	Fine here
Jan 17 14:41:12 <antarus>	fine for me so far
Jan 17 14:41:16 <SwifT>	fine here too
Jan 17 14:41:41 <NeddySeagoon>	robbat2: I won't know until my sister has booker here Holiday.  JUne 19 is the middle weekend
Jan 17 14:42:10 <robbat2>	ok, let's discuss in a few months
Jan 17 14:42:16 <robbat2>	when you know
Jan 17 14:42:24 <NeddySeagoon>	ok
Jan 17 14:42:55 <robbat2>	Genfleetoo, who was emailing him, i thought we discussed it last meeting
Jan 17 14:42:56 <dabbott>	I will put the 12th for now
Jan 17 14:43:09 <NeddySeagoon>	Gentoo Foundation. A dormant UK IT company operating from the same address as the Gentoo Housing Association.   I need to drop them a wee ditty still, just to point out the possibility of confusion
Jan 17 14:43:41 <NeddySeagoon>	We have already had one email for the Housing Association
Jan 17 14:44:11 <NeddySeagoon>	     Date of Next Meeting - 21 Feb 2016 19:00 UTC 
Jan 17 14:44:29 <dabbott>	ok here
Jan 17 14:44:35 <NeddySeagoon>	works for me. 
Jan 17 14:44:38 <SwifT>	wfm
Jan 17 14:44:42 <antarus>	sgtm
Jan 17 14:44:47 <robbat2>	good for me
Jan 17 14:44:57 <NeddySeagoon>	Any other business ... robbat2 
Jan 17 14:45:37 <robbat2>	CapitalOne has given me some flack about being a non-american holding access to a US account
Jan 17 14:46:09 <robbat2>	i want to see if they have solids grounds to do so, and/or find us a more accessible bank
Jan 17 14:46:42 <NeddySeagoon>	robbat2: go ahead. 
Jan 17 14:46:48 <antarus>	do we have a particular reason to be with them? I thought the main one was at some point they had a decent interest rate?
Jan 17 14:47:25 <robbat2>	the only reason we were with them, afaik, is that they got our ING stuff by acquisitions
Jan 17 14:47:28 <robbat2>	and we just stayed
Jan 17 14:47:31 <antarus>	I see
Jan 17 14:47:39 <NeddySeagoon>	robbat2: is it easier if we have a US citizen on the account too?
Jan 17 14:47:41 <quantumsummers>	yes, that is how I understand it as well
Jan 17 14:47:42 <dabbott>	robbat2: you have my ack
Jan 17 14:47:59 <robbat2>	NeddySeagoon: we need a US citizen regardless, but it's having me as an extra person that's the question
Jan 17 14:48:12 <robbat2>	specifically, a US resident must be one of the account holders
Jan 17 14:48:14 <antarus>	I think dabbott and I volunteered to be the US citizens if required
Jan 17 14:48:35 <robbat2>	yes, that's why I have docs for you to sign antarus (and neddy, as president)
Jan 17 14:49:00 <robbat2>	antarus, dabbott: if you have suggestions for a more accessible/useful bank
Jan 17 14:49:04 <robbat2>	i'm all ears
Jan 17 14:49:08 <quantumsummers>	apologies, but I have to go now. I am about to be over-run. I will be in touch tomorrow.
Jan 17 14:49:11 <robbat2>	i don't know much of the US banking environment
Jan 17 14:49:14 <robbat2>	quantumsummers: thanks
Jan 17 14:49:18 <NeddySeagoon>	Heh, I bet hey won't like me as a non US person either 
Jan 17 14:49:22 <dabbott>	thanks quantumsummers :)
Jan 17 14:49:25 <antarus>	robbat2: any specific requirements on accessability?
Jan 17 14:49:35 <NeddySeagoon>	thank you quantumsummers 
Jan 17 14:50:03 <robbat2>	antarus: online access, cheques, easy online bank transfers
Jan 17 14:50:19 <robbat2>	the last one is the hardest on capitalone
Jan 17 14:50:40 <antarus>	offhand, I'd say I've heard great things about merrill lynch, but I will ask
Jan 17 14:50:48 <antarus>	my housemate is particularly resourceful in this area
Jan 17 14:50:54 <NeddySeagoon>	shop around for a mare user friendly bank
Jan 17 14:51:08 <robbat2>	we're bringing ~$100k of account
Jan 17 14:51:33 <robbat2>	most of which is just savings that we'd like interest on
Jan 17 14:51:49 <antarus>	yeah the other thought was coming up with a more robust investment plan
Jan 17 14:51:53 <antarus>	but will consider later
Jan 17 14:52:12 <robbat2>	that's later
Jan 17 14:52:25 <robbat2>	after some other stuff gets off the ground (from my election platform)
Jan 17 14:52:38 <NeddySeagoon>	antarus: Investments are not our primary business.  So yes, but carefully
Jan 17 14:53:07 <robbat2>	[ref: foundation financially supporting/aiding more organized gentoo events]
Jan 17 14:53:34 <NeddySeagoon>	Any more 'other business' ?
Jan 17 14:53:38 <NeddySeagoon>	antarus: ?
Jan 17 14:53:43 <antarus>	negative
Jan 17 14:53:43 <robbat2>	none from me
Jan 17 14:53:52 <NeddySeagoon>	dabbott: ?
Jan 17 14:53:53 <dabbott>	none here
Jan 17 14:54:03 <NeddySeagoon>	none here
Jan 17 14:54:05 <NeddySeagoon>	quantumsummers: 
Jan 17 14:54:13 <SwifT>	none here either
Jan 17 14:54:24 <NeddySeagoon>	SwifT: Oops ...
Jan 17 14:54:56 <dabbott>	I got the log, minutes etc, starting a new folder 2016 :P
Jan 17 14:55:08 <NeddySeagoon>	Responsibilities ... dabbott you will post the log?  There are  no motions or emails
Jan 17 14:55:18 <NeddySeagoon>	Open Floor ...
Jan 17 14:56:25 *	NeddySeagoon bangs the virtual gavel to close the meeting.
Jan 17 14:56:32 <NeddySeagoon>	Thank you team
Jan 17 14:56:35 <robbat2>	thanks
Jan 17 14:56:48 <SwifT>	thank you
