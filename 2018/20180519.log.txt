[22:00:15] Meeting started by prometheanfire
[22:00:31] Meeting chairs are: prometheanfire, alicef, dabbott, kensington, klondike, 
[22:00:36] Current subject: roll call, (set by prometheanfire)
[22:00:39] <prometheanfire> o/
[22:00:44] <dabbott> here
[22:00:45] <alicef> o/
[22:01:03] <prometheanfire> quorum met
[22:01:09] <prometheanfire> kensington: klondike ?
[22:01:17] <prometheanfire> iirc kensington said he may not be able to make it
[22:02:18] <prometheanfire> will wait one more min before moving on
[22:03:38] <prometheanfire> ok, moving on
[22:03:51] Current subject: activity tracker, (set by prometheanfire)
[22:04:29] <prometheanfire> next item is to initiate the election, you ready to do that dabbott ?
[22:04:42] <prometheanfire> it's for june, but it'd be good to have to community be aware that it's coming up
[22:05:22] <dabbott> yes, we need to get a rough draft of the dates
[22:05:40] <dabbott> first we prune the member list
[22:05:52] <prometheanfire> procedure is doc'd here https://wiki.gentoo.org/wiki/Foundation:Elections#Procedure
[22:06:01] <dabbott> we can vote on that later in this meeting
[22:06:08] <prometheanfire> ok
[22:06:16] <prometheanfire> next then
[22:06:22] Current subject: alicef's items, (set by prometheanfire)
[22:06:38] LINK: https://wiki.gentoo.org/wiki/Foundation:Meetings/2018/05#alicef [Foundation:Meetings/2018/05 - Gentoo Wiki]
[22:06:39] <robbat2> (here, will comment re election process stuff later)
[22:06:43] <alicef> same no changes
[22:06:51] <prometheanfire> ok
[22:07:03] <prometheanfire> robbat2: I'll be asking about gdpr progress soon :D
[22:07:10] <prometheanfire> klondike: here?
[22:07:37] Current subject: prometheanfire's items, (set by prometheanfire)
[22:07:59] <prometheanfire> no progress on the openssl stuff, that's more on the technical side of gentoo to solve (make stable 1.1)
[22:08:59] <prometheanfire> gdpr is next, I didn't have time for this due to being gone for two weeks since the last meeting
[22:09:05] <prometheanfire> but maybe infra did more?
[22:09:07] <prometheanfire> jmbsvicetto: ping?
[22:09:30] <prometheanfire> jmbsvicetto: robbat2, do we know what info we have, so we can move forward with control of it?
[22:09:58] <robbat2> of devs, it's easy, LDAP has a lot, and they can get their own data back out of it
[22:10:10] <robbat2> the other services are a different matter
[22:10:23] <prometheanfire> do we have a list of those services?
[22:10:30] <robbat2> that's the service catalog
[22:10:55] <robbat2> what needs to happen with it is identify which services collect more than just IP
[22:11:18] <prometheanfire> ip is considered personal if it can be tied back to the individual iirc
[22:11:20] <robbat2> it needs somebody with a lot of time, which I don't have
[22:11:30] <prometheanfire> don't think anyone has that time :|
[22:13:02] <prometheanfire> ok, well, I guess we (foundation) need to ask infra formally for that info, since we do need to be in compliance
[22:13:08] <robbat2> on the plus side, very few of our services go out of their way to deliberately collect stuff
[22:13:17] <robbat2> forums collects the most
[22:13:31] <robbat2> wiki & bugs collect just a little like name
[22:13:43] <veremitz> I don't think it matters the how or why or how frequently .. :P
[22:14:18] <prometheanfire> it's important that we move forward on this though
[22:14:29] <veremitz> 25th May is teh deadline
[22:14:32] <robbat2> there will be zero progress until somebody has time
[22:14:36] <robbat2> deadline or not
[22:14:48] <prometheanfire> yep
[22:15:12] <veremitz> wait til the foundation gets sued again I guess ..
[22:15:12] <veremitz> that'll sharpen a few wits
[22:15:33] <veremitz> </stfu>
[22:15:39] <robbat2> veremitz: we cannot be personally sued for it, we can have a fine levied by the EU body
[22:15:52] <robbat2> personally = the foundation
[22:15:55] <robbat2> but that's not sueing
[22:16:04] <prometheanfire> fine can be quite high
[22:16:11] <prometheanfire> it's in the millions
[22:16:15] <robbat2> yes, i'm ware of the fine cap
[22:16:22] <prometheanfire> 10% or millions, whichever is greater
[22:16:24] <robbat2> greater of 20% revenue or 20M EUR or some
[22:16:34] <prometheanfire> ah, 20
[22:16:34] <robbat2> that's the maximum amount
[22:16:44] <robbat2> that the fine is allowed to be
[22:16:51] <robbat2> which does NOT mean every fine will be that
[22:16:57] <prometheanfire> true
[22:17:38] <robbat2> i'll tell people to poke at the service catalog, but that's the best I can do, due to nobody having time for this project
[22:17:42] <robbat2> continue for the moment
[22:17:44] <prometheanfire> sure
[22:18:05] Current subject: community items, (set by prometheanfire)
[22:18:13] LINK: https://wiki.gentoo.org/wiki/Foundation:Meetings/2018/05#Community_Items [Foundation:Meetings/2018/05 - Gentoo Wiki]
[22:18:35] <dabbott> lets vote on the community items so they don't keep comming up each month
[22:18:51] <dabbott> I vote 1 => 6 no 7 yes
[22:19:14] <prometheanfire> ok, ya, I meant to clean those up
[22:19:16] <robbat2> didn't we vote on a bunch last month
[22:19:22] <dabbott> I can email eben if needed
[22:20:29] <prometheanfire> dabbott: that'd be appreciated, I'd like the trustees to review before it's sent though
[22:20:42] <dabbott> OK
[22:21:03] <robbat2> i'm not a trustee anymore, but I believe #6 (reopen_nominations) is reasonable
[22:21:24] <dabbott> robbat2: we had a conference call with him or was that someone else
[22:21:32] <prometheanfire> I agree, I think it can be done next session though
[22:21:35] <alicef> prometheanfire: ok
[22:21:38] <dabbott> was that bradley
[22:21:38] <prometheanfire> this election is too close
[22:22:00] <robbat2> and for #3, infra is under the foundation already, just say fine
[22:22:12] <robbat2> i don't think it's too close, and we don't need to amend bylaws to do it
[22:22:53] <prometheanfire> ok, on 1, I don't think it's a good idea to be tied down
[22:22:59] <prometheanfire> therefore, no for me on 1
[22:23:26] <prometheanfire> formalizing the org structure (2) is also a no for now, given the other things we have in the air
[22:23:55] <prometheanfire> control over infra is already done, so going to remove it, (no need for vote)
[22:23:59] <prometheanfire> that's (3)
[22:24:21] <prometheanfire> (4) trustees enforce CoC on council
[22:25:10] <prometheanfire> I vote no, that's not our job, if something legal comes up we handle it because of that, not the CoC (which is not a legal doc)
[22:25:25] <prometheanfire> (5) Trustees place user representitive on the council
[22:25:39] <prometheanfire> no, that's not our job, it'd also mess up glep 39 a ton
[22:25:46] <robbat2> comment re #4: everybody, incl. council & trustees should be respecting the CoC
[22:26:07] <prometheanfire> robbat2: should be, but we are not the enforcement mechanism
[22:26:08] <robbat2> you don't get a pass just because you're on some management body
[22:26:20] <robbat2> the enforcement mechanism is comrel
[22:26:31] <prometheanfire> yep
[22:26:36] <prometheanfire> (6) Add reopen nominations option to ballot
[22:26:55] <robbat2> there was a timeline posted for reopen_nominations that fit with the original timeline
[22:27:13] <prometheanfire> if we can make the change now the I vote yes
[22:27:29] <robbat2> condense the original process from 2 months to 6 weeks, and use the extra 2 weeks for a 2nd round if needed
[22:27:33] <prometheanfire> even if not I vote yes, it'll simply be implimented as soon as possible
[22:28:07] <prometheanfire> (7) contact sflc, yes (dabbott is doing it)
[22:28:10] <robbat2> can we approve it and say exact timeline to be announced, but will keep to the approximate original schedule
[22:28:14] <dabbott> please send an email to trustees with the election time line for this election
[22:28:26] <dabbott> so I can get started
[22:28:39] <robbat2> mgorny: can you talk to me for the timeline to reopen_noms later?
[22:28:54] <robbat2> comment re scrubbing membership lists: i have my tooling almost done for the csv->yaml change
[22:29:01] <dabbott> needs to be done for the AGM
[22:29:05] <robbat2> it does show clearly we don't have many changes in teh list
[22:29:32] <prometheanfire> we good to move on?  robbat2 dabbott and mgorny will work on the move to the new election scheme
[22:29:44] Current subject: infra update, (set by prometheanfire)
[22:29:55] <prometheanfire> robbat2: can you give that ( jmbsvicetto seems absent )
[22:30:25] <robbat2> there was some hardware death due to old-age this past month
[22:30:42] <robbat2> some of stuff we own, others of sponsor-owned
[22:30:59] <robbat2> the original dipper died, we moved the content to blackcap (and renamed)
[22:31:05] <robbat2> but the replacement was the same age
[22:31:16] <robbat2> dipper runs the core rsync/distfiles generation
[22:31:31] <robbat2> sometime in the future it will need to move
[22:31:42] <robbat2> possibly to newly purchased hardware
[22:31:54] <robbat2> it's not suited for the present VM hosting at OSL
[22:32:10] <robbat2> because it's IO&CPU-heavy, and we don't want it co-located with untrusted VMs
[22:32:34] <robbat2> might write a proposal to buy a pair of hypervisors for trusted VMs, to be at OSL
[22:32:52] <dabbott> Now is the time to upgrade while we have the funds 
[22:32:53] <robbat2> would incl. a 10Gbit switch
[22:33:11] <prometheanfire> ok
[22:34:07] Current subject: open bugs, (set by prometheanfire)
[22:34:54] <prometheanfire> I don't see much
[22:35:19] <robbat2> to reduce the bus factor, is anybody among present trustees interested in learning at least some of the accounting/treasurer stuff?
[22:35:44] <robbat2> i'd be teaching you as I taught zlg
[22:35:50] <prometheanfire> if I wasn't planning wedding stuff I would
[22:35:51] <robbat2> in return for help keeping it up to date
[22:35:58] <prometheanfire> I'd say to ask me in a year :|
[22:36:12] <robbat2> this offer also open to non-trustees who might be considering running next term
[22:36:13] <dabbott> I could do it if alicef wants to take secretary duties
[22:36:26] <alicef> dabbott: yes
[22:37:09] <robbat2> (i'll take more than one person if that presents itself)
[22:38:00] <dabbott> robbat2: I don't have a background in accounting but could be used to keep stuff up to date untill we get someone more qualified 
[22:38:05] <robbat2> comments re financials, so it goes into minutes
[22:38:45] <robbat2> i haven't had time to spend on it since last month, but the exchange stuff is mostly done, what remains is depreciation & value of donated services
[22:38:51] <robbat2> plus data entry for this most recent year
[22:39:23] <prometheanfire> ok
[22:39:37] Current subject: new business, (set by prometheanfire)
[22:39:39] <dabbott> tbh I just don't have much time, I work alot
[22:39:55] <dabbott> I would be afraid i would fall behind
[22:40:05] <prometheanfire> foundation removal of members
[22:40:14] LINK: https://bugs.gentoo.org/653904 [653904 – Foundation member removal candidates (based on 2016+2017 elections)]
[22:40:15] <robbat2> (i have new business)
[22:40:32] <dabbott> mgornys list looks good, what ablut zlg
[22:40:40] <robbat2> that list isn't accurate
[22:40:50] <robbat2> the yaml will show more detail later
[22:41:13] <prometheanfire> ya, I assume you are going to be helping with that then?
[22:41:17] <robbat2> yes
[22:41:25] <dabbott> robbat2: thanks
[22:42:30] Current subject: council requests our vote on https://archives.gentoo.org/gentoo-nfp/message/b985f7c13359f521c451322dae59ebf7, (set by prometheanfire)
[22:43:22] <prometheanfire> I don't think we need to vote, but I think it's an ok change
[22:43:50] <robbat2> unofficial +1 for it from me
[22:44:01] <dabbott> fine by me
[22:44:52] <prometheanfire> dilfridge: there's your ack
[22:45:06] Current subject: copyright assignment, (set by prometheanfire)
[22:45:11] LINK: https://archives.gentoo.org/gentoo-nfp/message/45d2eafb4416db744ab1e0af708534c5 [Re: [gentoo-nfp] Trustee meeting Meetings&#x2F;2018&#x2F;05 agenda - gentoo-nfp - Gentoo Mailing List Archives]
[22:46:20] <dabbott> alicef: you a member of the copyright team
[22:46:35] <dabbott> I defer to the copyright team
[22:47:48] <prometheanfire> I think question 'a' would need to counsult with a lawyer
[22:47:52] <alicef> i don't remeber to have signed anything in 2003/2004 ...
[22:48:16] <robbat2> unofficial answer as part of the people working on that glep:
[22:48:22] <prometheanfire> successor to the GTI is hard
[22:48:33] <robbat2> 1. we ARE the assignee of GTI
[22:48:52] <robbat2> 1.1. but the dates of the documents mean some future obligations may still exist
[22:49:14] <robbat2> 1.2. the release was specifically future obligations, needs review for current/past obligations
[22:49:48] <robbat2> 2. I had proposed a futhur general release from the Foundation
[22:49:51] <alicef> it also say that as been nullified
[22:50:03] <robbat2> 2.1. mostly as CYA for parts that wern't missed
[22:50:15] <robbat2> 2.2. but to be rolled towards the new optional FLA work
[22:50:46] <prometheanfire> robbat2: yep, that's my understanding, I guess we'll work with the copyright team (ulm, alicef) on this
[22:51:40] <alicef> ok
[22:52:19] <prometheanfire> so I suppose the answer there is that work is ongoing
[22:52:46] <robbat2> i'd ask that somebody draft the further release, and post to the lists for review
[22:53:03] <robbat2> to collect that input, and take it to an IP lawyer for review
[22:53:08] <robbat2> after community review
[22:53:15] <prometheanfire> sure
[22:53:34] <prometheanfire> it does sound like that's the right way forward given the timeline
[22:54:04] <prometheanfire> ok, taking that
[22:54:26] Current subject: date of next meeting, (set by prometheanfire)
[22:54:29] <prometheanfire> Saturday, June 16 2018 22:00 UTC ?
[22:54:38] <robbat2> i have my new business stuff still
[22:54:42] <robbat2> when you're ready for it
[22:54:42] <prometheanfire> oh, right
[22:54:50] <prometheanfire> open floor for that
[22:54:51] <robbat2> i will be on a plane during that next meeting
[22:54:52] <prometheanfire> which is next
[22:55:17] <dabbott> June 16 is ok here
[22:55:22] <prometheanfire> I'm fine moving it a week later or earlier
[22:55:38] <robbat2> no
[22:55:43] <robbat2> later is better
[22:55:45] <robbat2> not earlier
[22:55:51] <robbat2> but I realise it impacts recording date
[22:55:55] <robbat2> because that's next meeting
[22:56:26] <dabbott> June 23 is ok here
[22:56:52] <prometheanfire> alicef: june 23 good for you?
[22:56:57] <alicef> yes
[22:57:00] <prometheanfire> ok
[22:57:05] <prometheanfire> june 23 it is
[22:57:11] Current subject: open floor, (set by prometheanfire)
[22:57:14] <prometheanfire> robbat2: you're up
[22:57:48] <robbat2> forums mods were approached, informally by email, by law enforcement
[22:57:53] <robbat2> with a request for user information
[22:58:06] <robbat2> this request was passed on to infra/trustees
[22:58:26] <robbat2> i believe this is the first such request that we've had
[22:59:14] <robbat2> we need to draft a response that a formal request (subpeona, warrant) is required for user info
[22:59:27] <robbat2> and be ready to recieve that formal request
[22:59:33] <robbat2> it may come with a data preservation order
[23:00:13] <robbat2> a number of large tech firms have good guides for law enforcement on that
[23:00:19] <robbat2> and I think we should crib heavily
[23:00:36] <robbat2> unless we can find an even more closely fitting documetn for open source orgs
[23:00:48] <prometheanfire> sounds good
[23:00:57] <robbat2> this should be foundation writing the policy doc here
[23:00:58] <robbat2> not infra
[23:01:24] <prometheanfire> agreed
[23:01:48] <veremitz> EBUSFACTOR :/
[23:01:58] <prometheanfire> I'll submit a doc by next meeting
[23:02:15] <dabbott> prometheanfire: thanks
[23:02:19] <robbat2> the LE agent should have a response not later than end of next week
[23:02:34] <prometheanfire> neat
[23:02:42] <robbat2> i shared some potential wording out of band for whomever is going to write that
[23:02:49] <robbat2> i'll happily review that, but don't have time to write it
[23:02:59] <prometheanfire> neither do I really
[23:03:07] <prometheanfire> but what choice do I have :|
[23:04:28] <prometheanfire> ok, anyone have anything else?
[23:04:38] <dabbott> robbat2: that was homeland security asking
[23:05:21] <robbat2> i was not specifying which agency
[23:05:40] <robbat2> it's a almalgamated mess
[23:05:51] <robbat2> (the US federal LE orgs)
[23:06:33] <prometheanfire> yep
[23:06:53] Current subject: ending, (set by prometheanfire)
[23:07:01] <prometheanfire> dabbott: you are posting the logs/minutes
[23:07:09] <dabbott> got it
[23:07:12] <prometheanfire> alicef: you'll update the motions
[23:07:20] <robbat2> request re logs
[23:07:23] <prometheanfire> dabbott: no emails to send, this time
[23:07:24] <alicef> yes
[23:07:35] <robbat2> the old logs had terrible file naming
[23:07:36] <prometheanfire> I'll update the agenda (done) and topic
[23:07:47] <robbat2> can you please start using ISO date naming for files?
[23:07:52] <robbat2> and go back to fix old logs?
[23:07:59] <prometheanfire> I'd like that too
[23:08:05] <robbat2> i'll help dig up missing old logs as well if you find the dates of meetings for me
[23:08:08] <dilfridge> how un-american
[23:08:24] <robbat2> i have an almost complete history of this channel since 2008/01/11
[23:08:31] Meeting ended by prometheanfire, total meeting length 4096 seconds
